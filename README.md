# HTTP/2 With Node.js 🤷‍♂️

Talk presented at [FOSSASIA 2017](http://2017.fossasia.org/tracks.html#2896) about the status of HTTP/2 support in Node.js.

## Video

[![Thumbnail preview of video: HTTP2 With Nodejs](http://img.youtube.com/vi/_7FfJ-4BzbY/0.jpg)](https://www.youtube.com/watch?v=_7FfJ-4BzbY&list=PLzZVLecTsGpLhEwJlgVLP-36T7b8AjRG1&index=128 "Live recording of the talk: HTTP2 With Nodejs")

Thanks to [Engineers.sg](https://engineers.sg/conference/fossasia-2017). ❤️

## Slides

- [HTML live preview](https://sebdeckers.gitlab.io/talk-http2-with-nodejs/)
- [PDF](https://gitlab.com/sebdeckers/talk-http2-with-nodejs/raw/master/HTTP2%20With%20Nodejs.pdf)
- [Keynote (source)](https://gitlab.com/sebdeckers/talk-http2-with-nodejs/raw/master/HTTP2%20With%20Nodejs.key)

## See Also

- [The State of HTTP/2 in Node with James Snell at Node.js Interactive 2016](https://changelog.com/spotlight/9)
- [Implementing HTTP/2 for Node.js Core by James Snell, IBM](https://www.youtube.com/watch?v=7uNGKCao8gA&list=PLfMzBWSH11xYaaHMalNKqcEurBH8LstB8&index=40)
