const {readFileSync} = require('fs')
const {createSecureServer} = require('http2')
const options = {
  key: readFileSync('key.pem'),
  cert: readFileSync('cert.pem')
}
createSecureServer(options, (request, response) => {
  response.writeHead(200, {
    'content-type': 'text/html; charset=utf-8'
  })
  response.end('<h1>Hello, World! 🎉</h1>')
}).listen(8443)